;; companion function to cljlet that recursively 
(defun twopairs (list)
  (if (not list)
      nil
      (cons (cons (car list) (cons (cadr list) nil)) (twopairs (cddr list)))))

(defmacro cljlet (bindings &body body)
  `(let ,(twopairs bindings) ,@body))
