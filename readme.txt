This is a toy macro that allows you to use let with Clojure-like binding.

Ex:

(cljlet (a 2 b 3)
   (* a b)) ;; => 6

Might be buggy; I'm new to lisp and even newer to macro-writing.
